package it.venis.iris2;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;

import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;


public class SmtpMailSender {
	private JavaMailSender javaMailSender;

	public SmtpMailSender(JavaMailSender javaMailSender) {
		super();
		this.javaMailSender = javaMailSender;
	}

	
	public void send(String[] to, String subject, String body, String from, String attachment, String[] listCC, String[] listCCN, String nomeFile, boolean bConfermaLettura)
		throws MessagingException {

		MimeMessage message = javaMailSender.createMimeMessage();		
		if (bConfermaLettura) message.setHeader("Disposition-Notification-To", from);
		
		MimeMessageHelper helper;
		
		helper = new MimeMessageHelper(message, true, "UTF-8");

		if (from != null)
			try {
				helper.setFrom(from, "NOME_MITTENTE");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		if (subject != null) helper.setSubject(subject);
		if (to != null) helper.setTo(to);		
		
		if (attachment != null)
		{
			DataSource source = new FileDataSource(attachment);
            helper.addAttachment(nomeFile, source);
		}
		
		if (listCC != null)	helper.setCc(listCC);
		if (listCCN != null) helper.setBcc(listCCN);				
		helper.setText(body, true);
		
		javaMailSender.send(message);
	}
}

