package it.venis.iris2.db.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.transaction.annotation.Transactional;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import it.venis.iris2.AppConfigDbIris;
import it.venis.iris2.AppConfigITB;
import it.venis.iris2.controller.ApiController;
import it.venis.iris2.elastic.utils.CElasticSUtils;
import it.venis.iris2.irisinterface.DatiCooperatore;
import it.venis.iris2.oggettidbiris.VfixAssegnazione;
import it.venis.iris2.oggettidbiris.VfixCommenti;
import it.venis.iris2.oggettidbiris.VfixConcluse;
import it.venis.iris2.oggettidbiris.VfixEmailUrp;
import it.venis.iris2.oggettidbiris.VfixEmailUrpExample;
import it.venis.iris2.oggettidbiris.VfixEmailUrpMapper;
import it.venis.iris2.oggettidbiris.VfixEndpointNotifiche;
import it.venis.iris2.oggettidbiris.VfixEndpointNotificheExample;
import it.venis.iris2.oggettidbiris.VfixEndpointNotificheMapper;
import it.venis.iris2.oggettidbiris.VfixFotografie;
import it.venis.iris2.oggettidbiris.VfixFotografieExample;
import it.venis.iris2.oggettidbiris.VfixFotografieMapper;
import it.venis.iris2.oggettidbiris.VfixInopportune;
import it.venis.iris2.oggettidbiris.VfixIterSegnalazione;
import it.venis.iris2.oggettidbiris.VfixLogMimuv;
import it.venis.iris2.oggettidbiris.VfixLogMimuvExample;
import it.venis.iris2.oggettidbiris.VfixLogMimuvMapper;
import it.venis.iris2.oggettidbiris.VfixNoteDk;
import it.venis.iris2.oggettidbiris.VfixNoteDkMapper;
import it.venis.iris2.oggettidbiris.VfixRefEsterno;
import it.venis.iris2.oggettidbiris.VfixRefEsternoMapper;
import it.venis.iris2.oggettidbiris.VfixReferente;
import it.venis.iris2.oggettidbiris.VfixReferenteMapper;
import it.venis.iris2.oggettidbiris.VfixSegnalazione;
import it.venis.iris2.oggettidbiris.VfixSegnalazioneExample;
import it.venis.iris2.oggettidbiris.VfixSegnalazioneMapper;
import it.venis.iris2.oggettidbiris.VfixSegnalazione_SIT;
import it.venis.iris2.oggettidbiris.VfixSitoSegnalazione;
import it.venis.iris2.oggettidbiris.VfixSitoSegnalazioneExample;
import it.venis.iris2.oggettidbiris.VfixSitoSegnalazioneMapper;
import it.venis.iris2.oggettidbiris.VfixTmpMimuv;
import it.venis.iris2.oggettidbiris.VfixUtenteRegistrato;
import it.venis.iris2.oggettidbiris.VfixUtenteRegistratoExample;
import it.venis.iris2.oggettidbiris.VfixUtenteRegistratoMapper;

import org.springframework.web.bind.annotation.RequestParam;

import it.venis.iris2.oggettidbiris.dao.CustomMapper;
import it.venis.iris2.pushnotification.CWebPush;
import it.venis.iris2.security.CGestioneUtente;
import it.venis.iris2.utils.CCoopApplicativa;
import it.venis.iris2.utils.CEMailMessaggi;
import it.venis.iris2.utils.CUtils;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

//@Transactional(rollbackFor = Exception.class)

public class CDbUtils {
	private AnnotationConfigApplicationContext ctx;
	private AnnotationConfigApplicationContext ctx2;

	private Properties props;
	private String ambiente = "";

	public CDbUtils() {
		// TODO Auto-generated constructor stub
		java.util.ResourceBundle mybundle = java.util.ResourceBundle
				.getBundle("application");
		ambiente = mybundle.getString("ambiente");
		props = new Properties();
		try {
			FileInputStream input = new FileInputStream(new ClassPathResource(
					".\\messaggi.properties").getFile());
			props.load(new InputStreamReader(input, Charset
					.forName("ISO-8859-1")));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		ctx = new AnnotationConfigApplicationContext();
		ctx.register(AppConfigDbIris.class);
		ctx.refresh();
		
		ctx2 = new AnnotationConfigApplicationContext();
		ctx2.register(AppConfigITB.class);
		ctx2.refresh();
		
	}

	protected void finalize() throws Throwable {
		super.finalize();
		ctx.close();
		ctx2.close();
	}

	public String InserisciDatisuDB(String datiSegnalazione,
			String sAuthorization, boolean bExternal) {

		String sJson = "";
		sJson = datiSegnalazione;

		JsonParser parser = new JsonParser();
		JsonObject jsonObj = (JsonObject) parser.parse(sJson);
		jsonObj = jsonObj.getAsJsonObject("datiSegnalazione");

		// TODO - controllo dei dati ricevuti (validazione server)
		// TODO - controllo dell'account - per ora simuliamo un nuovo utente
		// TODO - la parte send to mimuv deve essere gestita in qualche maniera,
		// ma a livello più globale. In questo modo soddisferà l'interfaccia
		// universale
		// TODO - le notifiche, se servono, a segnalazione salvata
		// TODO - le mail agli uffici ed al segnalatore, a segnalazione salvata

		CustomMapper cm = ctx.getBean(CustomMapper.class);

		CUtils ut = new CUtils();

		/*
		 * DataSourceTransactionManager dst = (DataSourceTransactionManager)
		 * ctx.getBean("transactionManager"); TransactionDefinition txDef = new
		 * DefaultTransactionDefinition(); TransactionStatus txStatus =
		 * dst.getTransaction(txDef); DefaultTransactionDefinition def = new
		 * DefaultTransactionDefinition(); // explicitly setting the transaction
		 * name is something that can only be done programmatically
		 * def.setName("SomeTxName");
		 * def.setPropagationBehavior(TransactionDefinition
		 * .PROPAGATION_REQUIRED); TransactionStatus status =
		 * dst.getTransaction(def);
		 */

		try {

			// controllo preventivo che la tipologia di problema sia trattata
			// dalla municipalità selezionata! -- 20.07.18
			JsonObject jp = ((JsonObject) (jsonObj.get("POSIZIONE")));
			JsonObject joCiv = leggiInfoCivico(jp.get("LATITUDINE")
					.getAsString(), jp.get("LONGITUDINE").getAsString());
			if (joCiv != null) {
				Short sQuanti = cm.bPrbTrattatoDaMunic(
						jsonObj.get("FK_TIPOLOGIA").getAsShort(),
						joCiv.get("SCOM_COD").getAsShort());
				if (sQuanti == 0) {
					return "{\"Errore\":\""
							+ props.getProperty("strPrbNotMunic") + "\"}";
				}
			}

			VfixSegnalazione newSegn = null;
			// controlla se l'utente esiste già e lo riusa, oppure ne crea uno
			// nuovo...
			String sExtToken = null;
			if (bExternal) // recupera autorizzazione dai dati che vengono
							// passati...
			{
				JsonObject joSegn = jsonObj.get("SEGNALATORE")
						.getAsJsonObject();

				sAuthorization = joSegn.get("COGNOME").getAsString() + "|"
						+ joSegn.get("NOME").getAsString() + "|"
						+ joSegn.get("CF").getAsString() + "|"
						+ joSegn.get("EMAIL").getAsString() + "|";
						//+ joSegn.get("TELEFONO").getAsString() + "|"
				sAuthorization = "#EXT#" + sAuthorization;
				sExtToken = joSegn.get("CF").getAsString();
			}

			VfixUtenteRegistrato utente = oSettaUtente(sAuthorization, sExtToken);

			newSegn = new VfixSegnalazione();
			newSegn.setDataSegnalazione(new Date());
			newSegn.setAppName("IRIS");
			newSegn.setPtrUtente(utente.getIdUtenteRegistrato());
			newSegn.setFkTipologia(jsonObj.get("FK_TIPOLOGIA").getAsInt());
			newSegn.setSubject(jsonObj.get("SUBJECT").getAsString());
			newSegn.setIndicazioni(jsonObj.get("INDICAZIONI").getAsString());
			newSegn.setNcOk(jsonObj.get("NC_OK").getAsString());
			newSegn.setDevice(jsonObj.get("DEVICE").getAsString());

			try {
				cm.insertSegnalazione(newSegn);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return "{\"Errore\":\"" + props.getProperty("strErrore")
						+ "\"}";
			}

			VfixSitoSegnalazione newSS = new VfixSitoSegnalazione();
			// JsonObject jp = ((JsonObject) (jsonObj.get("POSIZIONE")));
			newSS.setPtrSegnalazione(newSegn.getIdSegnalazione());
			newSS.setLatitudine(jp.get("LATITUDINE").getAsBigDecimal());
			newSS.setLongitudine(jp.get("LONGITUDINE").getAsBigDecimal());

			if (joCiv != null) {
				newSS.setIndirizzoSito(joCiv.get("COMPLETO").getAsString());
				newSS.setIndirizzoSistema(joCiv.get("COMPLETO").getAsString());
				newSS.setCodVia(joCiv.get("CODICE_VIA").getAsLong());
				newSS.setCodCivico(joCiv.get("CIVICO").getAsLong());
				newSS.setBarrato(joCiv.get("LETTERA").getAsString() != "" ? joCiv
						.get("LETTERA").getAsString() : "_");
				newSS.setPtrMunic(joCiv.get("SCOM_COD").getAsShort());
			}

			try {
				cm.insertSitoSegnalazione(newSS);
			} catch (Exception e) {
				e.printStackTrace();
				cm.cancellaSegnalazioneCompleta(newSegn);
				return "{\"Errore\":\"" + props.getProperty("strErrore")
						+ "\"}";
			}

			// inserimento delle fotografie...

			JsonArray arrFoto = jsonObj.get("FOTO").getAsJsonArray();
			List<String> urls = new ArrayList<String>();

			for (JsonElement pa : arrFoto) {
				// salvataggio della foto sul db
				VfixFotografie foto = new VfixFotografie();

				foto.setPtrSegnalazione(newSegn.getIdSegnalazione());
				foto.setInterno(pa.getAsJsonObject().get("INTERNO")
						.getAsString());
				String sNomeFile = ut.sComponiNomeFileImmagine(pa
						.getAsJsonObject().get("NOME_FILE").getAsString());
				foto.setNomeFile(sNomeFile);
				foto.setNomeFileThumb(sNomeFile);
				foto.setNomeFileThumbMid(sNomeFile);
				// salvataggio della foto sul server

				try {
					cm.insertFotografia(foto);
				} catch (Exception e) {
					e.printStackTrace();
					cm.cancellaSegnalazioneCompleta(newSegn);
					return "{\"Errore\":\"" + props.getProperty("strErrore")
							+ "\"}";
				}

				ut.bCopiaRemota(sNomeFile, pa.getAsJsonObject().get("IMMAGINE")
						.getAsString()); // scrive il file in remoto con il nome
											// uguale...

				urls.add(foto.getNomeFile());

				System.out.println(pa);
			}

			// inserisce la nuova segnalazione sul layer del SIT
			try {
				if (!ambiente.equalsIgnoreCase("TEST"))
					modificaITB(newSegn.getIdSegnalazione(), true);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				cm.cancellaSegnalazioneCompleta(newSegn);
				return "{\"Errore\":\"" + props.getProperty("strErrore")
						+ "\"}";
			}

			// TODO - Rilegge dal db ed inserire in ElasticSearch --- OLTRE IL
			// COMMIT
			componilistasegnalazioni(newSegn.getIdSegnalazione().toString(),
					false);

			// operazioni complementari...

			try {
				// esportaSegnalazione(newSegn.getIdSegnalazione()); // 10.10.18
				esportaSegnalazione(newSegn, utente, newSS, CUtils.N_OP_INOLTRATA, bExternal); // 10.10.18
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// segnalazioni via email...
			// ------------- ai referenti coinvolti

			List<Map<String, String>> destinatari;
			CEMailMessaggi emsg = new CEMailMessaggi();
			if (newSS.getInfoExt() != null
					&& (!newSS.getInfoExt().contains("|")))
				destinatari = cm.getListaReferenti_Ext(
						newSegn.getFkTipologia(), newSS.getPtrMunic(),
						newSS.getInfoExt());
			else
				destinatari = cm.getListaReferenti_Ext(
						newSegn.getFkTipologia(), newSS.getPtrMunic(), "NULL");

			if (destinatari.size() > 0) {
				String sTesto = ut.sStrMessaggio(newSegn, utente, true);
				emsg.inviaEMail(destinatari, sTesto, "Reg. nr. "
						+ newSegn.getIdSegnalazione().toString() + " - "
						+ newSegn.getSubject().toString().trim());
			}

			// ------------- all'utente segnalatore

			String sEMailUtente = ut.getEMailUtente(utente);
			if (sEMailUtente != null && !sEMailUtente.equals("")
					&& !sEMailUtente.equals("-")) {
				String sTesto = ut.sStrMessaggio(newSegn, utente, false);
				if (!sTesto.equals(""))
					new CEMailMessaggi().inviaEMail(sEMailUtente, "", sTesto, "Reg. nr. "
							+ newSegn.getIdSegnalazione().toString() + " - "
							+ newSegn.getSubject().toString().trim());
			}

			Long lIdUser = newSegn.getPtrUtente();
			CWebPush wp2 = new CWebPush();
			List<VfixEndpointNotifiche> le = this.selectEndpoint(lIdUser
					.toString()); // poi potrò indicare lo user
			for (VfixEndpointNotifiche theEp : le) {
				wp2.doIt(theEp.getEndpoint(),
						"{\"body\":\"Nuova segnalazione Iris (nr. "
								+ newSegn.getIdSegnalazione().toString()
								+ ") inserita!\", \"idsegnalazione\":\""
								+ newSegn.getIdSegnalazione().toString()
								+ "\" }");
			}

			// -- ricava l'ultimo ufficio... informazione che serve a Salesforce e quindi la metto per tutti
			String szReferente = sUltimoUfficio(newSegn.getIdSegnalazione());
			
			sJson = "{\"NuovoId\":" + newSegn.getIdSegnalazione().toString() + ",\"Ufficio\":\"" + szReferente + "\",\"Stato\":\"10\"}";

		} catch (Exception e) {
			e.printStackTrace();
			return "{\"Errore\":\"" + props.getProperty("strErrore") + "\"}";
		}

		return sJson;
	}

	// ------

	public String InserisciDatiCommento(String datiCommento) {

		String sJson = datiCommento;

		JsonParser parser = new JsonParser();
		JsonObject jsonObj = (JsonObject) parser.parse(sJson);
		jsonObj = jsonObj.getAsJsonObject("datiCommento");

		// TODO - controllo dei dati ricevuti (validazione server)
		// TODO - controllo dell'account - per ora simuliamo un nuovo utente
		// TODO - le notifiche, se servono, a segnalazione salvata
		// TODO - le mail agli uffici ed al segnalatore, a segnalazione salvata

		CustomMapper cm = ctx.getBean(CustomMapper.class);
		try {
			VfixCommenti commento = new VfixCommenti();

			commento.setPtrSegnalazione(jsonObj.get("PTR_SEGNALAZIONE")
					.getAsLong());
			commento.setFirma("");
			commento.setInterno(jsonObj.get("INTERNO").getAsString());
			commento.setIdea("N");
			commento.setTesto(jsonObj.get("TESTO").getAsString());
			commento.setTitolo(jsonObj.get("TITOLO").getAsString());
			commento.setData(new Date());

			cm.insertCommento(commento);
			sJson = "{\"Commento inserito\":\"" + commento.getTitolo() + "\"}";
			
			try {
				CEMailMessaggi emsg = new CEMailMessaggi();
				emsg.bEMailOperatoriCommento (commento, emsg.COMM_AGGIUNTO);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			sJson = "{\"Errore\":\"" + props.getProperty("strErrore") + "\"}";
		} finally {
		}

		// TODO - Rileggere dal db ed inserire in ElasticSearch --- OLTRE IL
		// COMMIT
		componilistasegnalazioni(jsonObj.get("PTR_SEGNALAZIONE").getAsString(),
				true); // rimpiazza il precedente e rilegge!!!

		return sJson;
	}

	// ------

	public String EliminaCommento(String datiCommento) {

		String sJson = datiCommento;

		JsonParser parser = new JsonParser();
		JsonObject jsonObj = (JsonObject) parser.parse(sJson);
		jsonObj = jsonObj.getAsJsonObject("datiCommento");

		CustomMapper cm = ctx.getBean(CustomMapper.class);
		try {
			VfixCommenti commento = new VfixCommenti();
			commento.setPtrSegnalazione(jsonObj.get("PTR_SEGNALAZIONE")
					.getAsLong());
			commento.setProgr(jsonObj.get("PROGR").getAsShort());
			commento.setData(new Date());
			cm.deleteCommento(commento);
			sJson = "{\"Commento eliminato\":\"" + commento.getTitolo() + "\"}";
			
			try {
				CEMailMessaggi emsg = new CEMailMessaggi();
				emsg.bEMailOperatoriCommento (commento, emsg.COMM_RIMOSSO);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			sJson = "{\"Errore\":\"" + props.getProperty("strErrore") + "\"}";
		} finally {
		}
		// TODO - Rileggere dal db ed inserire in ElasticSearch --- OLTRE IL
		// COMMIT
		componilistasegnalazioni(jsonObj.get("PTR_SEGNALAZIONE").getAsString(),
				true); // rimpiazza il precedente e rilegge!!!
		return sJson;
	}

	// ------

	public String componilistasegnalazioni(
			@RequestParam(value = "idsegnalazione", defaultValue = "") String sIdSegnalazione,
			@RequestParam(value = "bUpdate", defaultValue = "false") boolean bUpdate) {
		String sJson = "";
		CustomMapper cm = ctx.getBean(CustomMapper.class);

		List<Map<String, String>> listaSegn = null;
		List<Map<String, String>> theIter = null;
		List<Map<String, String>> theFoto = null;
		List<Map<String, String>> theCommenti = null;
		CElasticSUtils esU = new CElasticSUtils();

		if (sIdSegnalazione.isEmpty()) {
			esU.bResettaIndice(true);
			listaSegn = cm.selectListaSegnalazioni(0L);
		} else
			listaSegn = cm.selectListaSegnalazioni(Long
					.parseLong(sIdSegnalazione));

		String sLocation = "{\"lon\": [LON], \"lat\": [LAT]}";
		JsonParser parser = new JsonParser();
		JsonObject oSegnalazione = null;

		for (int i = 0; i < listaSegn.size(); i++) {
			sJson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping()
					.create().toJson(listaSegn.get(i));
			oSegnalazione = parser.parse(sJson).getAsJsonObject();

			theIter = cm.selectIterSegnalazioneMasked(oSegnalazione.get(
					"ID_SEGNALAZIONE").getAsLong());
			JsonElement jeIter = parser.parse(new GsonBuilder()
					.setPrettyPrinting().disableHtmlEscaping().create()
					.toJson(theIter));
			oSegnalazione.add("ITER", jeIter);

			theFoto = cm.selectFotoSegnalazione(oSegnalazione.get(
					"ID_SEGNALAZIONE").getAsLong());
			JsonElement jeFoto = parser.parse(new GsonBuilder()
					.setPrettyPrinting().disableHtmlEscaping().create()
					.toJson(theFoto));
			oSegnalazione.add("FOTO", jeFoto);

			theCommenti = cm.selectCommentiSegnalazione(oSegnalazione.get(
					"ID_SEGNALAZIONE").getAsLong());
			JsonElement jeCommenti = parser.parse(new GsonBuilder()
					.setPrettyPrinting().disableHtmlEscaping().create()
					.toJson(theCommenti));
			oSegnalazione.add("COMMENTI", jeCommenti);

			JsonElement jeLocation = parser.parse(sLocation.replace("[LAT]",
					oSegnalazione.get("LATITUDINE").getAsString()).replace(
					"[LON]", oSegnalazione.get("LONGITUDINE").getAsString()));

			oSegnalazione.add("location", jeLocation);

			System.out.println(i + " --- " + oSegnalazione.toString());

			if (bUpdate)
				esU.bCancellaDati(oSegnalazione.get("ID_SEGNALAZIONE")
						.getAsLong());
			esU.bInserisciDati(oSegnalazione.toString());
		}
		return sJson;
	}

	public boolean inserisciNuovoEndpoint(String endpoint, String useragent) {
		try {
			if (endpoint.equals("{}"))
				return false;
			VfixEndpointNotificheMapper cm = ctx
					.getBean(VfixEndpointNotificheMapper.class);
			VfixEndpointNotificheExample eex = new VfixEndpointNotificheExample();
			eex.createCriteria().andEndpointEqualTo(endpoint);
			List<VfixEndpointNotifiche> le = cm.selectByExample(eex);
			if (le.size() > 0) {
				le.get(0).setIdUtente(
						new CGestioneUtente().getCurrentUserIrisID());
				le.get(0).setValido("S");
				cm.updateByExampleSelective(le.get(0), eex);
				return false;
			}

			VfixEndpointNotifiche sNewEndpoint = new VfixEndpointNotifiche();
			sNewEndpoint.setIdUtente(new CGestioneUtente()
					.getCurrentUserIrisID());
			sNewEndpoint.setEndpoint(endpoint);
			sNewEndpoint.setUserAgent(useragent);
			sNewEndpoint.setValido("S");
			sNewEndpoint.setData(new Date());
			cm.insert(sNewEndpoint);

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
		}

		return true;
	}

	public boolean disabilitaEndpoint(String endpoint) {

		try {
			if (endpoint.equals("{}"))
				return false;
			VfixEndpointNotificheMapper cm = ctx
					.getBean(VfixEndpointNotificheMapper.class);
			VfixEndpointNotificheExample eex = new VfixEndpointNotificheExample();
			eex.createCriteria().andEndpointEqualTo(endpoint);
			List<VfixEndpointNotifiche> le = cm.selectByExample(eex);
			if (le.size() > 0) {
				le.get(0).setValido("N");
				cm.updateByExampleSelective(le.get(0), eex);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
		}
		return true;
	}

	public List<VfixEndpointNotifiche> selectEndpoint(String idUser) {
		VfixEndpointNotificheExample eex = new VfixEndpointNotificheExample();
		if (idUser != null)
			eex.createCriteria().andIdUtenteEqualTo(idUser);

		VfixEndpointNotificheMapper cm = ctx
				.getBean(VfixEndpointNotificheMapper.class);
		try {
			return cm.selectByExample(eex);

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	// eliminazione logica (lato front) della segnalazione
	public String EliminaSegnalazioneLogica(String datiInopportuna) {

		String sJson = datiInopportuna;

		JsonParser parser = new JsonParser();
		JsonObject jsonObj = (JsonObject) parser.parse(sJson);
		
		try {
			jsonObj = (jsonObj.getAsJsonObject("datiInopportuna") == null)? jsonObj : jsonObj.getAsJsonObject("datiInopportuna");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		CustomMapper cm = ctx.getBean(CustomMapper.class);
		try {
			VfixInopportune inopportuna = new VfixInopportune();
			inopportuna.setPtrSegnalazione(jsonObj.get("PTR_SEGNALAZIONE")
					.getAsLong());
			inopportuna.setMotivo(jsonObj.get("MOTIVO").getAsString());
			inopportuna.setData(new Date());
			cm.rimuoviSegnalazioneLogica(inopportuna);
			// TODO - Elimina da ElasticSearch
			CElasticSUtils esU = new CElasticSUtils();
			esU.bCancellaDati(inopportuna.getPtrSegnalazione()); // la rimuove
																	// da
																	// ElasticSearch
			sJson = "{\"Segnalazione rimossa\":"
					+ inopportuna.getPtrSegnalazione().toString() + "}";
			
			try {
				CEMailMessaggi emsg = new CEMailMessaggi();
				emsg.bEMailOperatoriInopportuna(inopportuna);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			sJson = "{\"Errore\":\"" + props.getProperty("strErrore") + "\"}";
		}

		return sJson;
	}

	// eliminazione logica (lato front) della segnalazione
	public String modificaITB(Long lIdSegnalazione, boolean bInsert) {
		List<VfixSegnalazione_SIT> ls;

		CustomMapper cm = ctx.getBean(CustomMapper.class);
		CustomMapper cm2 = ctx2.getBean(CustomMapper.class);
		try {
			ls = cm.selectforSIT(lIdSegnalazione);
			if (bInsert)
				cm2.inserisciforSIT(ls.get(0));
			else
				cm2.updateforSIT(ls.get(0));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
		}

		return null;
	}

	// cancellazione dal sit
	public String deleteITB(Long lIdSegnalazione) {
		AnnotationConfigApplicationContext ctx2;

		ctx2 = new AnnotationConfigApplicationContext();
		ctx2.register(AppConfigITB.class);
		ctx2.refresh();

		CustomMapper cm2 = ctx2.getBean(CustomMapper.class);
		try {
			cm2.deleteforSIT(lIdSegnalazione);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			ctx2.close();
		}
		return null;
	}

	public String dtGetDate(Date value) {
		String szdate = null;
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(
					"dd/MM/yyyy HH:mm:ss");
			szdate = formatter.format(value);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return szdate;

	}

	public BigDecimal bdGetBigDecimal(String value) {
		if (value.isEmpty())
			return null;
		String sAux = value.replace(".", "").replace(",", ".");
		return new BigDecimal(sAux);

	}

	// eliminazione logica (lato front) della segnalazione
	public String getNearestPoint(String Lat, String Lon) {
		AnnotationConfigApplicationContext ctx2;

		ctx2 = new AnnotationConfigApplicationContext();
		ctx2.register(AppConfigITB.class);
		ctx2.refresh();

		CustomMapper cm2 = ctx2.getBean(CustomMapper.class);
		try {
			Map<String, String> lista = cm2.nearestPoint(Lat, Lon);
			return new GsonBuilder().setPrettyPrinting().disableHtmlEscaping()
					.create().toJson(lista);
		} catch (Exception e) {
			e.printStackTrace();
			ctx2.close();
		} finally {
			ctx2.close();
		}

		return null;
	}

	public String componilistamarker() {
		AnnotationConfigApplicationContext ctx2;

		ctx2 = new AnnotationConfigApplicationContext();
		ctx2.register(AppConfigITB.class);
		ctx2.refresh();

		CustomMapper cm = ctx.getBean(CustomMapper.class);
		CustomMapper cm2 = ctx2.getBean(CustomMapper.class);

		List<VfixSegnalazione_SIT> ls;

		// cm2.deleteforSIT (0L);
		ls = cm.selectforSIT(0L);

		for (int i = 0; i < ls.size(); i++) {
			try {
				cm2.inserisciforSIT(ls.get(i));
				Thread.sleep(200);
				System.out.println("Inserito Sit: #" + i);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		ctx2.close();
		System.out.println("Finito");
		return "";
	}

	// --------------------------------------------------------------------
	// --------------------------------------------------------------------

	public String sAggiornaIter(String datiOperazione) {
		// TODO: il client deve verificare che lo stato nuovo non sia in
		// conflitto con il precedente (es., posso chiudere solo se in carico).

		String sJson = "";
		boolean bConMail = false;

		String szUtente = new CGestioneUtente().getCurrentUser(); // qui va
																	// recuperato
																	// l'utente!!!

		sJson = datiOperazione;

		JsonParser parser = new JsonParser();
		JsonObject jsonObj = (JsonObject) parser.parse(sJson);
		jsonObj = jsonObj.getAsJsonObject("datiOperazione");

		if (jsonObj.get("NOTE").getAsString().length() > 1000)
			return "{\"Errore\":\"Le note non possono superare i 1000 caratteri!\"}";

		VfixIterSegnalazione nuovoStato = new VfixIterSegnalazione();
		nuovoStato.setPtrSegnalazione(jsonObj.get("ID").getAsLong());
		nuovoStato.setIdRiga(0L);
		nuovoStato.setPtrUtente(szUtente); // TODO: attenzione che l'utente è
											// quello loggato in Iris2
		nuovoStato.setPtrTipoStato(jsonObj.get("STATO").getAsShort());
		nuovoStato.setNote(jsonObj.get("NOTE").getAsString().trim());

		VfixConcluse datiConcl = null;
		VfixAssegnazione datiTrasf = null;

		CustomMapper cm = ctx.getBean(CustomMapper.class);
		String emailSegnalatore = cm
				.getEmailSegnalatore(jsonObj.get("ID").getAsLong()).get(0)
				.get("EMAIL").toString();

		// ricava la municipalità di appartenenza
		VfixSitoSegnalazione ss = ctx.getBean(VfixSitoSegnalazioneMapper.class)
				.selectByPrimaryKey(jsonObj.get("ID").getAsLong());

		switch (jsonObj.get("STATO").getAsByte()) {
		case CUtils.N_OP_RISOLTA: {
			bConMail = true;
			datiConcl = new VfixConcluse();
			datiConcl.setPtrSegnalazione(jsonObj.get("ID").getAsLong());
			datiConcl.setPtrSubtipo(jsonObj.get("SUBSTATO").getAsShort());

			break;
		}
		case CUtils.N_OP_IN_CARICO: {
			datiTrasf = new VfixAssegnazione();
			datiTrasf.setPtrSegnalazione(jsonObj.get("ID").getAsLong());
			String sMunicList = cm.sLeggiListaMunic(szUtente).get(0)
					.get("LISTA_MUN");
			Integer ptrRefer;
			try {
				ptrRefer = ((BigDecimal) (Object) cm
						.bControllaAbilitUtente(jsonObj.get("ID").getAsLong(),
								szUtente, sMunicList).get(0).get("PTR_REFER"))
						.intValue();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				ptrRefer = 0;
			}
			datiTrasf.setPtrRef(ptrRefer);
			datiTrasf.setPtrRiga(0L);

			break;
		}

		case CUtils.N_OP_RESPINTA: // occhio che la SOSPESA è inibita!
		{
			bConMail = true;
			datiTrasf = new VfixAssegnazione();
			datiTrasf.setPtrSegnalazione(jsonObj.get("ID").getAsLong());

			/*
			 * attenzione: qui si considera sempre l'esistenza di una darkroom.
			 * Altrimenti non può essere riassegnata (l'operazione, infatti, non
			 * è tra quelle eseguibili dal front di Iris2
			 */
			datiTrasf.setPtrRef(CUtils.REF_DEFAULT_DARKROOM);
			datiTrasf.setPtrRiga(0L);

			break;
		}

		default:
			break;
		}

		// scrive...
		// il tutto sarà una transazione...
		cm.updateStatoSegnalazione(nuovoStato);

		if (datiTrasf != null) {
			datiTrasf.setPtrRiga(nuovoStato.getIdRiga());
			cm.updateStatoAssegnazione(datiTrasf);
		}

		if (datiConcl != null)
			cm.insertInfoConclusa(datiConcl);

		VfixLogMimuv datiPrgEsterno = new VfixLogMimuv();
		datiPrgEsterno.setPtrSegnalazione(jsonObj.get("ID").getAsLong());
		datiPrgEsterno.setValido("N");
		cm.updateLogPrgEsterno(datiPrgEsterno);

		// inserimento delle fotografie aggiunte dall'operatore
		CUtils ut = new CUtils();
		JsonArray arrFoto = jsonObj.get("FOTO").getAsJsonArray();
		for (JsonElement pa : arrFoto) {
			// salvataggio della foto sul db
			VfixFotografie foto = new VfixFotografie();
			foto.setPtrSegnalazione(jsonObj.get("ID").getAsLong());
			foto.setInterno("S"); // sicuramente interna
			String sNomeFile = ut.sComponiNomeFileImmagine(pa.getAsJsonObject()
					.get("NOME_FILE").getAsString());
			foto.setNomeFile(sNomeFile);
			foto.setNomeFileThumb(sNomeFile);
			foto.setNomeFileThumbMid(sNomeFile);
			// salvataggio della foto sul server
			try {
				cm.insertFotografia(foto);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return "{\"Errore\":\"" + props.getProperty("strErrore")
						+ "\"}";
			}

			ut.bCopiaRemota(sNomeFile, pa.getAsJsonObject().get("IMMAGINE")
					.getAsString()); // scrive il file in remoto con il nome
										// uguale...
			System.out.println(pa);
		}

		// TODO email notifica segnalatore

		// TODO email notifica operatori

		// TODO notifica del cambio di stato a programma esterno (nella
		// fattispecie, Mimuv)
		// TODO notifica del cambiamento di stato + aggiornamento della
		// segnalazione in ElasticSearch...
		componilistasegnalazioni(nuovoStato.getPtrSegnalazione().toString(),
				true);
		modificaITB(nuovoStato.getPtrSegnalazione(), false);
		
		// --- NOTIFICA DI AGGIORNAMENTO PER SALESFORCE -- 13.12.18
		CCoopApplicativa coop = new CCoopApplicativa();
		coop.bAggiorna_per_Esterno(nuovoStato.getPtrSegnalazione(), "SALESFORCE");
		// --- Fine -- 13.12.18

		return sJson;
	}

	// legge info del civico da SIT
	private JsonObject leggiInfoCivico(String lat, String lon) {
		String sJson = getNearestPoint(lat, lon);
		JsonParser parser = new JsonParser();
		JsonObject jsonObj = (JsonObject) parser.parse(sJson);
		return jsonObj;
	}

	public List<VfixUtenteRegistrato> selectUtenteRegistrato(String theToken) {
		if (theToken == null)
			return null;
		VfixUtenteRegistratoExample eex = new VfixUtenteRegistratoExample();
		eex.createCriteria().andTokenEqualTo(theToken);
		VfixUtenteRegistratoMapper cm = ctx
				.getBean(VfixUtenteRegistratoMapper.class);
		try {
			return cm.selectByExample(eex);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public VfixUtenteRegistrato selectUtenteRegistratoByCF(String theCF) {
		if (theCF == "")
			return null;
		VfixUtenteRegistratoExample eex = new VfixUtenteRegistratoExample();
		eex.createCriteria().andTokenIsNotNull().andCodFiscLike(theCF);
		VfixUtenteRegistratoMapper cm = ctx
				.getBean(VfixUtenteRegistratoMapper.class);
		try {
			return cm.selectByExample(eex).get(0);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public VfixUtenteRegistrato selectUtenteRegistratoByIdSegn(Long theIDSegn) {
		VfixSegnalazione theSegn = ctx.getBean(VfixSegnalazioneMapper.class)
				.selectByPrimaryKey(theIDSegn);
		if (theSegn == null)
			return null;
		VfixUtenteRegistratoMapper cm = ctx
				.getBean(VfixUtenteRegistratoMapper.class);
		try {
			if (theSegn.getPtrUtente() != null)
				return cm.selectByPrimaryKey(theSegn.getPtrUtente());
			return null;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public VfixUtenteRegistrato selectUtenteRegistratoById(Long theID) {
		VfixUtenteRegistratoMapper cm = ctx
				.getBean(VfixUtenteRegistratoMapper.class);
		try {
			if (theID != null)
				return cm.selectByPrimaryKey(theID);
			return null;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public VfixUtenteRegistrato selectUtenteRegistratoByBearer(String theBearer) {
		if (theBearer == "")
			return null;
		VfixUtenteRegistratoExample eex = new VfixUtenteRegistratoExample();
		eex.createCriteria().andTokenIsNotNull().andBearerEqualTo(theBearer);
		VfixUtenteRegistratoMapper cm = ctx
				.getBean(VfixUtenteRegistratoMapper.class);
		try {
			return cm.selectByExample(eex).get(0);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	// --- utente abilitato o meno per una segnalazione...
	public String bControllaAbilitUtente(Long theSegn, String theUser) {
		CustomMapper cm = ctx.getBean(CustomMapper.class);

		try {
			String sMunicList = cm.sLeggiListaMunicipalita(theUser);
			if (sMunicList == null || sMunicList.equals(""))
				return null;
			return cm.sControllaAbilitUtente(theSegn, theUser, sMunicList);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	// id del segnalatore
	public Long getIdSegnalatore(Long IdSegnalazione) {
		JsonParser parser = new JsonParser();
		JsonObject oDati = null;

		try {
			CustomMapper cm = ctx.getBean(CustomMapper.class);
			List<Map<String, String>> infoSegnalatore = cm
					.getInfoSegnalatore(IdSegnalazione);

			String sJson = new GsonBuilder().setPrettyPrinting()
					.disableHtmlEscaping().create()
					.toJson(infoSegnalatore.get(0));
			oDati = parser.parse(sJson).getAsJsonObject();

			return oDati.get("ID_UTENTE_REGISTRATO").getAsLong();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	// crea un nuovo utente all'atto della conferma per le notifiche
	// TODO: l'utente deve essere completato!!!
	public VfixUtenteRegistrato oSettaUtente(String sAuthorization, String sExtToken) {
		CustomMapper cm = ctx.getBean(CustomMapper.class);
		CGestioneUtente ut = new CGestioneUtente();
		
		boolean bUrpTokenOn = false;

		VfixUtenteRegistrato utente = ut.getCurrentUserIris(sExtToken);
		if (utente == null) {
			utente = new VfixUtenteRegistrato();
			utente.setCognome("-");
			utente.setNome("-");
			utente.setEmail("-");
			utente.setTelefono("-");
			utente.setCodFisc("-");

			if (ut.sGetDatiBySpid(sAuthorization))
				utente.setCodFisc(ut.getsCF()); // nel caso sia SPID
			else if (ut.sGetDatiByDiMe(sAuthorization))
				utente.setCodFisc(ut.getsCF()); // nel caso sia DIME
			else if (ut.sGetDatiByUncredited(sAuthorization)) {
				utente.setNome(ut.getsNome()); // nel caso sia libero accesso
				utente.setCognome(ut.getsCognome());
				utente.setEmail(ut.getsEMail());
				bUrpTokenOn = vCheckIfURP(ut.getsEMail()); 
			}

			if (sExtToken == null) 
			{
				if (!bUrpTokenOn)
					utente.setToken(ut.getCurrentUser());
				else
					utente.setToken(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
			}
			else utente.setToken(sExtToken);
			
			utente.setBearer(sAuthorization);
			try {
				cm.insertUtente(utente);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}
		return utente;
	}

	public boolean checkMia(Long idSegnalazione, Long idSegnalatore) {
		VfixSegnalazioneExample eex = new VfixSegnalazioneExample();
		eex.createCriteria().andIdSegnalazioneEqualTo(idSegnalazione)
				.andPtrUtenteEqualTo(idSegnalatore);
		VfixSegnalazioneMapper cm = ctx.getBean(VfixSegnalazioneMapper.class);
		try {
			if (cm.selectByExample(eex).size() > 0)
				return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
		return false;
	}

	public boolean esportaSegnalazione(Long idSegnalazione, byte nTipoOp, boolean bExternal) {
		VfixSegnalazione segn = ctx.getBean(VfixSegnalazioneMapper.class)
				.selectByPrimaryKey(idSegnalazione);
		if (segn == null)
			return false;
		VfixSitoSegnalazione sito = ctx.getBean(
				VfixSitoSegnalazioneMapper.class).selectByPrimaryKey(
				idSegnalazione);
		VfixUtenteRegistrato ut = getInfoSegnalatore(idSegnalazione);
		VfixFotografieExample eex = new VfixFotografieExample();
		eex.createCriteria().andPtrSegnalazioneEqualTo(idSegnalazione);
		List<VfixFotografie> lFoto = ctx.getBean(VfixFotografieMapper.class)
				.selectByExample(eex);
		CCoopApplicativa coop = new CCoopApplicativa();
		coop.bDoCooperazione(segn, ut, sito, lFoto, nTipoOp, bExternal);
		return false;
	}

	public boolean esportaSegnalazione(VfixSegnalazione segn, VfixUtenteRegistrato utente,
			VfixSitoSegnalazione sito, byte nTipoOp, boolean bExternal) {
		VfixUtenteRegistrato ut = getInfoSegnalatore(segn.getIdSegnalazione());
		VfixFotografieExample eex = new VfixFotografieExample();
		eex.createCriteria()
				.andPtrSegnalazioneEqualTo(segn.getIdSegnalazione());
		List<VfixFotografie> lFoto = ctx.getBean(VfixFotografieMapper.class)
				.selectByExample(eex);
		CCoopApplicativa coop = new CCoopApplicativa();
		coop.bDoCooperazione(segn, ut, sito, lFoto, nTipoOp, bExternal);
		return false;
	}

	public String leggiRefEsterni(Long idReferente) {
		VfixReferente ref = ctx.getBean(VfixReferenteMapper.class)
				.selectByPrimaryKey(idReferente);
		return ref.getRefEsterni();
	}

	public VfixRefEsterno leggiRefEsterniUrl(String sRefEsterno) {
		VfixRefEsterno ref = ctx.getBean(VfixRefEsternoMapper.class)
				.selectByPrimaryKey(sRefEsterno);
		return ref;
	}

	public boolean setLogEsterno(Long idSegnalazione, Long idEsterno,
			String sRefEsterno, Short sTipo, String sValido) {
		try {
			VfixLogMimuv datiLogEsterno = new VfixLogMimuv();
			datiLogEsterno.setData(new Date());
			datiLogEsterno.setEventoMimuv(idEsterno);
			datiLogEsterno.setPtrSegnalazione(idSegnalazione);
			datiLogEsterno.setRefEsterno(sRefEsterno);
			datiLogEsterno.setTipo(sTipo);
			datiLogEsterno.setValido(sValido);
			ctx.getBean(VfixLogMimuvMapper.class).insert(datiLogEsterno);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
	}

	// --------------------------------------------------------------------
	// --------------------------------------------------------------------

	public VfixUtenteRegistrato getInfoSegnalatore(Long lIdSegnalazione) 
	{
		VfixUtenteRegistrato ut = null;
		String sDatiUtente = "";
		String sSecret = "";

		if (lIdSegnalazione != 0)
			ut = new CDbUtils().selectUtenteRegistratoByIdSegn(lIdSegnalazione);
		else
			return null;

		// servirebbe fare un MD5...
		if (ut.getBearer() == null || ut.getBearer().equals("")) {
		} else {
			CGestioneUtente gu = new CGestioneUtente();
			if (gu.sGetDatiByUncredited(ut.getBearer())) {
				ut.setCodFisc(gu.getsCF());
				ut.setCognome(gu.getsCognome());
				ut.setNome(gu.getsNome());
				ut.setEmail(gu.getsEMail());
				ut.setTelefono(gu.getsTelefono());
			} else if (gu.sGetDatiByDiMe(ut.getBearer())) {
				ut.setCodFisc(gu.getsCF());
				ut.setCognome(gu.getsCognome());
				ut.setNome(gu.getsNome());
				ut.setEmail(gu.getsEMail());
				ut.setTelefono(gu.getsTelefono());
			} else if (gu.sGetDatiBySpid(ut.getBearer())) {
				ut.setCodFisc(gu.getsCF());
				ut.setCognome(gu.getsCognome());
				ut.setNome(gu.getsNome());
				ut.setEmail(gu.getsEMail());
				ut.setTelefono(gu.getsTelefono());
			} else {
				// operatore che si è collegato
			}
		}

		return ut;
	}

	
	// --------------------------------------------------------------------
	// --------------------------------------------------------------------

	public String sAggiornaIterByCoop(DatiCooperatore datiC) {
		// TODO: il client deve verificare che lo stato nuovo non sia in
		// conflitto con il precedente (es., posso chiudere solo se in carico).
		String sJson = "";
		boolean bConMail = false;
		
		Long lIdSegnalazione = Long.parseLong(datiC.getChiaveIris());
		
		CustomMapper cm = ctx.getBean(CustomMapper.class);

		Short nRefAttuale = cm.getUltimoAssegnatario(Long.parseLong(datiC.getChiaveIris()));
		String szReferente = cm.desRefSpecializzatoByIdSegn(Long.parseLong(datiC.getChiaveIris()), nRefAttuale);
		String szLastUser = cm.lastuserByIdSegn(Long.parseLong(datiC.getChiaveIris()));

		VfixIterSegnalazione nuovoStato = new VfixIterSegnalazione();
		nuovoStato.setPtrSegnalazione(Long.parseLong(datiC.getChiaveIris()));
		nuovoStato.setIdRiga(0L);
		nuovoStato.setPtrUtente(szLastUser);
		nuovoStato.setPtrTipoStato(Short.parseShort(datiC.getStato()));
		nuovoStato.setNote(datiC.getNote().trim());

		VfixConcluse datiConcl = null;
		VfixAssegnazione datiTrasf = null;

		VfixUtenteRegistrato ut = getInfoSegnalatore(lIdSegnalazione);
		String emailSegnalatore = ut.getEmail();

		// ricava la municipalità di appartenenza
		VfixSitoSegnalazione ss = ctx.getBean(VfixSitoSegnalazioneMapper.class).selectByPrimaryKey(lIdSegnalazione);

		switch (Byte.parseByte(datiC.getStato())) {
			case CUtils.N_OP_RISOLTA: {
				bConMail = true;
				datiConcl = new VfixConcluse();
				datiConcl.setPtrSegnalazione(lIdSegnalazione);
				Short sSubTipo = 0;
				if (datiC.getMotivazione() != null && datiC.getMotivazione() != "")
				{
					sSubTipo = Short.parseShort(datiC.getMotivazione());
					if (sSubTipo < 10) sSubTipo = (short) (sSubTipo * 10); 
				}
				datiConcl.setPtrSubtipo(sSubTipo);	
				break;
			}
			
			case CUtils.N_OP_IN_CARICO: {
				datiTrasf = new VfixAssegnazione();
				datiTrasf.setPtrSegnalazione(lIdSegnalazione);
				datiTrasf.setPtrRef(nRefAttuale.intValue());
				datiTrasf.setPtrRiga(0L);
				break;
			}

			case CUtils.N_OP_RESPINTA: // occhio che la SOSPESA è inibita!
			{
				bConMail = true;
				datiTrasf = new VfixAssegnazione();
				datiTrasf.setPtrSegnalazione(lIdSegnalazione);
	
				/*
				 * attenzione: qui si considera sempre l'esistenza di una darkroom.
				 * Altrimenti non può essere riassegnata (l'operazione, infatti, non
				 * è tra quelle eseguibili dal front di Iris2
				 */
				datiTrasf.setPtrRef(CUtils.REF_DEFAULT_DARKROOM);
				datiTrasf.setPtrRiga(0L);
				break;
			}
	
			default:
				break;
		}

		// scrive...
		// il tutto sarà una transazione...
		cm.updateStatoSegnalazione(nuovoStato);

		if (datiTrasf != null) {
			datiTrasf.setPtrRiga(nuovoStato.getIdRiga());
			cm.updateStatoAssegnazione(datiTrasf);
		}

		if (datiConcl != null)
			cm.insertInfoConclusa(datiConcl);

		if (Byte.parseByte(datiC.getStato()) != CUtils.N_OP_IN_CARICO)
		{
			VfixLogMimuv datiPrgEsterno = new VfixLogMimuv();
			datiPrgEsterno.setPtrSegnalazione(lIdSegnalazione);
			datiPrgEsterno.setValido("N");
			cm.updateLogPrgEsterno(datiPrgEsterno);
		}

		// inserimento delle fotografie aggiunte dall'operatore
		// no, nel caso di cooperazione???

		// TODO notifica del cambiamento di stato + aggiornamento della segnalazione in ElasticSearch...
		componilistasegnalazioni(nuovoStato.getPtrSegnalazione().toString(), true);
		if (!ambiente.equalsIgnoreCase("TEST"))	modificaITB(nuovoStato.getPtrSegnalazione(), false);
		
		// --- NOTIFICA DI AGGIORNAMENTO PER SALESFORCE -- 13.12.18
		CCoopApplicativa coop = new CCoopApplicativa();
		coop.bAggiorna_per_Esterno(nuovoStato.getPtrSegnalazione(), "SALESFORCE");
		// --- Fine -- 13.12.18
		
		
		if (bConMail)
		{
			CEMailMessaggi emsg = new CEMailMessaggi();
			if (Byte.parseByte(datiC.getStato()) == CUtils.N_OP_RISOLTA)
			{
				// TODO email notifica segnalatore
				try {
					if (emailSegnalatore != null && !emailSegnalatore.equals("") && !emailSegnalatore.equals("-"))
						emsg.sStrMessaggio_Finale (lIdSegnalazione, emailSegnalatore);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				
				try {
					// TODO notifica endpoint
					Long lIdUser = ut.getIdUtenteRegistrato();
					CWebPush wp2 = new CWebPush();
					List<VfixEndpointNotifiche> le = this.selectEndpoint(lIdUser.toString()); // poi potrò indicare lo user
					String sStato = "Conclusa";
					String payload = "{\"body\":\"Aggiornamento della segnalazione " + lIdSegnalazione + ": " + sStato + "\", \"idsegnalazione\": " + lIdSegnalazione + "}";
					for (VfixEndpointNotifiche theEp : le) {
						wp2.doIt(theEp.getEndpoint(), payload);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else
			{
				// TODO email notifica operatori
				try {
					emsg.bEMailOperatoriRiassegnata(datiC, szReferente);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}			
			}
		
		}

		return sJson;
	}
	
	// --------------------------------------------------------------------
		// --------------------------------------------------------------------

	public String sAggiornaPostOperazione(String datiOperazione) {
		String sJson = "";
		sJson = datiOperazione;

		JsonParser parser = new JsonParser();
		JsonObject jsonObj = (JsonObject) parser.parse(sJson);
		jsonObj = jsonObj.getAsJsonObject("datiOperazione");

		if (jsonObj.get("NOTE").getAsString().length() > 1000)
			return "{\"Errore\":\"Le note non possono superare i 1000 caratteri!\"}";

		CustomMapper cm = ctx.getBean(CustomMapper.class);

		// inserimento delle fotografie aggiunte dall'operatore
		CUtils ut = new CUtils();
		JsonArray arrFoto = jsonObj.get("FOTO").getAsJsonArray();
		
		String[] Foto = new String[arrFoto.size()];
		byte bIdx = 0;
		
		VfixFotografie foto = new VfixFotografie();
		for (JsonElement pa : arrFoto) {
			// salvataggio della foto sul db

			String sNomeFile = ut.sComponiNomeFileImmagine(pa.getAsJsonObject()
					.get("NOME_FILE").getAsString());
			ut.bCopiaRemota(sNomeFile, pa.getAsJsonObject().get("IMMAGINE")
					.getAsString()); // scrive il file in remoto con il nome
										// uguale...
			
			Foto[bIdx] = sNomeFile; bIdx++;
			System.out.println(pa);
		}
		
		return sJson;
	}

	
	public String sUltimoUfficio(Long lIdAss) {
		// -- ricava l'ultimo ufficio... informazione che serve a Salesforce e quindi la metto per tutti
		CustomMapper cm = ctx.getBean(CustomMapper.class);
		String szReferente = "-";
		try {
			Short nRefAttuale = cm.getUltimoAssegnatario(lIdAss);
			szReferente = cm.desRefSpecializzatoByIdSegn(lIdAss, nRefAttuale);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return szReferente;
	}

	
	  public boolean vCheckIfURP (String sEMail)
	    {
	    	VfixEmailUrpExample eex = new VfixEmailUrpExample();
			eex.createCriteria().andEmailEqualTo(sEMail.toLowerCase());
			VfixEmailUrpMapper cm1 = ctx.getBean(VfixEmailUrpMapper.class);
			List<VfixEmailUrp> ll = cm1.selectByExample(eex);
	    	return ll.size() > 0;
	    }

	  
	  
	   // utilizzata per l'aggiornamento di gruppo...
		public String componilistasegnalazioniGruppo(List<Map<String, String>> ll, boolean bUpdate) {
			String sJson = "";
			CustomMapper cm = ctx.getBean(CustomMapper.class);

			List<Map<String, String>> theIter = null;
			List<Map<String, String>> theFoto = null;
			List<Map<String, String>> theCommenti = null;
			CElasticSUtils esU = new CElasticSUtils();


			String sLocation = "{\"lon\": [LON], \"lat\": [LAT]}";
			JsonParser parser = new JsonParser();
			JsonObject oSegnalazione = null;

			for (int i = 0; i < ll.size(); i++) {
				sJson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping()
						.create().toJson(ll.get(i));
				oSegnalazione = parser.parse(sJson).getAsJsonObject();

				// theIter =
				// cm.selectIterSegnalazione(oSegnalazione.get("ID_SEGNALAZIONE").getAsLong());
				theIter = cm.selectIterSegnalazioneMasked(oSegnalazione.get(
						"ID_SEGNALAZIONE").getAsLong());
				JsonElement jeIter = parser.parse(new GsonBuilder()
						.setPrettyPrinting().disableHtmlEscaping().create()
						.toJson(theIter));
				oSegnalazione.add("ITER", jeIter);

				theFoto = cm.selectFotoSegnalazione(oSegnalazione.get(
						"ID_SEGNALAZIONE").getAsLong());
				JsonElement jeFoto = parser.parse(new GsonBuilder()
						.setPrettyPrinting().disableHtmlEscaping().create()
						.toJson(theFoto));
				oSegnalazione.add("FOTO", jeFoto);

				theCommenti = cm.selectCommentiSegnalazione(oSegnalazione.get(
						"ID_SEGNALAZIONE").getAsLong());
				JsonElement jeCommenti = parser.parse(new GsonBuilder()
						.setPrettyPrinting().disableHtmlEscaping().create()
						.toJson(theCommenti));
				oSegnalazione.add("COMMENTI", jeCommenti);

				JsonElement jeLocation = parser.parse(sLocation.replace("[LAT]",
						oSegnalazione.get("LATITUDINE").getAsString()).replace(
						"[LON]", oSegnalazione.get("LONGITUDINE").getAsString()));

				oSegnalazione.add("location", jeLocation);

				System.out.println(i + " --- " + oSegnalazione.toString());

				if (bUpdate)
					esU.bCancellaDati(oSegnalazione.get("ID_SEGNALAZIONE")
							.getAsLong());
				esU.bInserisciDati(oSegnalazione.toString());
				try {
					Thread.sleep(300);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			// esU.aggiornaIndice();
			return sJson;
		}

		public String componilistamarkerGenerico(List<Map<String, String>> ll) {
			CustomMapper cm = ctx.getBean(CustomMapper.class);
			
			List<VfixSegnalazione_SIT> ls;
			
			String sJson = "";
			JsonObject oSegnalazione = null;
			CustomMapper cm2 = ctx2.getBean(CustomMapper.class);
			JsonParser parser = new JsonParser();

			for (int i = 0; i < ll.size(); i++) {
				try {
					sJson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping()
							.create().toJson(ll.get(i));
					oSegnalazione = parser.parse(sJson).getAsJsonObject();
					ls = cm.selectforSIT(oSegnalazione.get("ID_SEGNALAZIONE").getAsLong());

					cm2.updateforSIT(ls.get(0));
					Thread.sleep(200);
					System.out.println("Aggiornato Sit: #" + i);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			System.out.println("Finito");
			return "";
		}

}
