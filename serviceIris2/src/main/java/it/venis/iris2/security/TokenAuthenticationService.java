package it.venis.iris2.security;

import java.io.IOException;
import java.util.Date;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

import static java.util.Collections.emptyList;

public class TokenAuthenticationService {

	  static final long EXPIRATIONTIME = 864_000_000; // 10 days
	  static final String SECRET = [SECRET string];
	  static final String TOKEN_PREFIX = "Bearer";
	  static final String HEADER_STRING = "Authorization";	 

	  static void addAuthentication(HttpServletResponse res, String username) {
	    String JWT = Jwts.builder()
	        .setSubject(username)
	        .setExpiration(new Date(System.currentTimeMillis() + EXPIRATIONTIME))
	        .signWith(SignatureAlgorithm.HS512, SECRET)
	        .compact();
	    res.setHeader("Access-Control-Allow-Origin", "*");
	    res.setHeader("Access-Control-Allow-Headers", "Authorization");
	    res.setHeader("Access-Control-Expose-Headers", "Authorization, xsrf-token");
	    res.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
	    res.addHeader(HEADER_STRING, TOKEN_PREFIX + " " + JWT); 
	    try {
			res.getWriter().write("{}");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  }

	  static Authentication getAuthentication(HttpServletRequest request) {
	    String token = request.getHeader(HEADER_STRING);
	    if (token != null) {
	    	
	      // parse the token.
	      try {
	    	// tentativo con utente diretto (user e password...)  
			String user = Jwts.parser()
			      .setSigningKey(SECRET)
			      .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
			      .getBody()
			      .getSubject();

			  return user != null ?
			      new UsernamePasswordAuthenticationToken(user, null, emptyList()) :
			      null;
			} catch (ExpiredJwtException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UnsupportedJwtException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (MalformedJwtException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SignatureException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	       CGestioneUtente gut = new CGestioneUtente();
	       // tentativo con token uncredited...
	       if (gut.sGetDatiByUncredited(token))
	       {
	    	   String user = gut.getsToken(); 
	    	   return user != null ?
	 			      new UsernamePasswordAuthenticationToken(user, null, emptyList()) :
	 			      null;
	       }

	    	// tentativo con Spid (user e password...)	      
	       if (gut.sGetDatiBySpid(token))
	       {
	    	   String user = gut.getsToken(); 
	    	   return user != null ?
	 			      new UsernamePasswordAuthenticationToken(user, null, emptyList()) :
	 			      null;
	       }
	       
	       // tentativo con token DiMe...
	       if (gut.sGetDatiByDiMe(token))
	       {
	    	   String user = gut.getsToken(); 
	    	   return user != null ?
	 			      new UsernamePasswordAuthenticationToken(user, null, emptyList()) :
	 			      null;
	       }
	      
	    }
	    return null;
	  }

}
